$(document).ready(function(){
    var language = 0; //0 for br, 1 for en
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            trans(xhttp);
        }
    }
    xhttp.open("GET", "xml/language.xml", true);
    xhttp.send();

    $("#br").click(function(){
        language = 0;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                trans(xhttp);
            }
        }
        xhttp.open("GET", "xml/language.xml", true);
        xhttp.send();
    });
    $("#en").click(function(){
        language = 1;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                trans(xhttp);
            }
        }
        xhttp.open("GET", "xml/language.xml", true);
        xhttp.send();
    });
    function trans(xml) {
        xmlDoc = xml.responseXML;
        if(language == 0){
            x = xmlDoc.getElementsByTagName('algoritmo')[0];
            y = x.childNodes[0];
            alg = y.nodeValue;
            x = xmlDoc.getElementsByTagName('description')[0];
            y = x.childNodes[0];
            desc = y.nodeValue;
            x = xmlDoc.getElementsByTagName('title')[0];
            y = x.childNodes[0];
            title = y.nodeValue;
            x = xmlDoc.getElementsByTagName('iniciar')[0];
            y = x.childNodes[0];
            iniciar = y.nodeValue;
        } else {
            x = xmlDoc.getElementsByTagName('algoritmo')[1];
            y = x.childNodes[0];
            alg = y.nodeValue;
            x = xmlDoc.getElementsByTagName('description')[1];
            y = x.childNodes[0];
            desc = y.nodeValue;
            x = xmlDoc.getElementsByTagName('title')[1];
            y = x.childNodes[0];
            title = y.nodeValue;
            x = xmlDoc.getElementsByTagName('iniciar')[1];
            y = x.childNodes[0];
            iniciar = y.nodeValue;
        }
        document.title = title;
        $("#alg").html(alg);
        $("#title").html(title);
        $("#desc").html(desc);
        $("#next").val(iniciar);
    }
    $("#next").click(function(){
        window.location = "app.html";
    });
});

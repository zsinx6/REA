Simulador do Algoritmo de CFS:

O REA funcionará como uma simulação do algoritmo, ou seja, vai executar as ações de escalonamento
de processos, podendo permitir interação com o usuário e apresentará por meio de texto em mais de um
idioma, o que está sendo realizado pelo algoritmo no momento.
Ao iniciar será possível escolher entre algumas árvores já prontas (com alguns processos já na árvore),
ou com 0 processos (árvore vazia), sendo necessário o usuário inserir novos processos manualmente.
Após iniciado o algoritmo, as ações do usuário incluem pausar, avançar e remover um processo.
A única forma de entrada de novos processos é pelo usuário, encerramento de processos também.

O algoritmo funciona utilizando a estrutura de dados Árvore Rubro Negra, uma árvore binária de busca balanceda. 
Ele se basea na ideia de que todos os processos devem rodar de forma justa no processador, então, cada processo tem um 
contator do tempo na CPU vruntime, que indica o tempo em que o processo usou na CPU. Dessa forma o processo mais a 
esquerda na árvore será o escalonado, pois ele é quem possui o menor vruntime, ao executar ele vai incrementando o
vruntime até que esse valor já não seja mais o menor valor na árvore, então o nó é reinserido na árvore e o processo se
repete até que os processos terminem de executar.

Como usar:
Deve-se abrir o index.html e então ler as instruções de uso, bem como o funcionamento do algoritmo. Ao passar o mouse sobre os controles, aparecerá a mensagem com o que ele faz.

Requisitos:
O REA foi desenvolvido em web, portanto deve-se ter apenas um navegador atualizado com suporte a javascript e jquery.


Simulation of CFS Algorithm:

The OER simulates the algorithm, schedule the process in the tree, allowing the user to interact with it, and will show text explanations
in more than one language. When started the user can choose among various sets configurations of tree (already inserted), 
or if needed, with a empty tree, for the user to add them by hand. After the algorithm started, the user can pause, add new process,
step forward and remove a process. The only way to remove and insert new process is by user action.

The algorithm uses a red-black tree (self-balancing binary search tree), and was built over the ideia of fairness by the CPU,
leaving every process with the same amount of time inside CPU, this is done by giving a vruntime to each process, that indicates the
time passed in the CPU by that process. By that way the left most process on the tree will be the one with the smaller value on vruntime,
then, will be scheduled for running, and the vruntime is increased while vruntime is the smaller in tree, then, if is no more the smaller, 
the node is reinserted on the tree and the algorithm repeats until has process to be scheduled.

How to use:
Open the index.html and read the instructions, and how the algorithm works. On mouse hover on controllers, a tooltip will be displayed with informations about how to use.

Requirements:
The OER was developed as a web application, so only needs a web browser updated with supports javascript and jquery.

$(document).ready(function(){
    var language = 0; //0 for br, 1 for en
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            trans(xhttp);
        }
    }
    xhttp.open("GET", "xml/language.xml", true);
    xhttp.send();

    $("#br").click(function(){
        language = 0;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                trans(xhttp);
            }
        }
        xhttp.open("GET", "xml/language.xml", true);
        xhttp.send();
    });
    $("#en").click(function(){
        language = 1;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                trans(xhttp);
            }
        }
        xhttp.open("GET", "xml/language.xml", true);
        xhttp.send();
    });
    function trans(xml) {
        xmlDoc = xml.responseXML;
        if(language == 0){
            x = xmlDoc.getElementsByTagName('dev')[0];
            y = x.childNodes[0];
            dev = y.nodeValue;
            x = xmlDoc.getElementsByTagName('voltar')[0];
            y = x.childNodes[0];
            back = y.nodeValue;
        } else {
            x = xmlDoc.getElementsByTagName('dev')[1];
            y = x.childNodes[0];
            dev = y.nodeValue;
            x = xmlDoc.getElementsByTagName('voltar')[1];
            y = x.childNodes[0];
            back = y.nodeValue;
        }
        $("#titleAbout").html(dev);
        $("#next").val(back);
    }
    $("#next").click(function(){
        window.location = "index.html";
    });
});

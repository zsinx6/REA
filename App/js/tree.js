$(document).ready(function(){
    $("#bf").hide();
    var language = 0; //0 for br, 1 for en
    var text1 = null;
    var text2 = null;
    var text3 = null;
    var text4 = null;
    var text5 = null;
    var text6 = null;
    var help1 = null;
    var help2 = null;
    var help3 = null;
    var help4 = null;
    var help5 = null;
    var help6 = null;
    var help7 = null;
    var tex = null;
    var run = null;
    var title = null;
    var textback = null;
    var textoatual = null;
    var xhttp = new XMLHttpRequest();
    var stack = new Array();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            trans(xhttp);
        }
    }
    xhttp.open("GET", "xml/language.xml", true);
    xhttp.send();

    function backup(){
        this.tree = null;
        this.isrunning = "";
        this.textinfo = "";
        this.state = "";
        this.removed = 0;
        this.min = 0;
        this.lang = 0;
        this.getTree = function(){
            return this.tree;
        };
        this.isRun = function(){
            return this.isrunning;
        };
        this.getTextinfo = function(){
            return this.textinfo;
        };
        this.getState = function(){
            return this.state;
        };
        this.getRemoved = function(){
            return this.removed;
        };
        this.getMin = function(){
            return this.min;
        };
        this.getLang = function(){
            return this.lang;
        }
    }
    var backupit = new backup();

    var stage = new Konva.Stage({
          container: "treeContainer", 
          width: 1024,
          height: 400
    });

    var layer = new Konva.Layer();

    var posX = [512, 362, 662, 287, 437, 587, 737];
    var posY = [50, 150, 150, 250, 250, 250, 250];
    var colors = ['#D40000', '#000000'];

    function drawNode(color, x, y, val){
        var group = new Konva.Group({
             x: x,
             y: y
        });

        var circle = new Konva.Circle({
          x: 0,
          y: 0,
          radius: 30,
          fill: "#FFFFFF",
          stroke: color,
          strokeWidth: 2
        });

        var innercircle = new Konva.Circle({
           x: 0,
           y: 0,
           radius: 25,
           fill: "#FFFFFF",
           stroke: "#000000",
           strokeWidth: 1
         });

        var text = new Konva.Text({
           x: -15,
           y: -7,
           text: val,
           fontSize: 16,
           fontFamily: 'Arial',
           fill: color,
           width: 30,
           align: 'center'
        });

        // var tooltip = new Konva.Shape({
        //     sceneFunc: function(context) {
        //            context.beginPath();
        //            context.moveTo(x+15, y+15);
        //            context.lineTo(x+10, y+25);
        //             context.lineTo(x-30, y+25);
        //             context.lineTo(x-30, y+85);
        //             context.lineTo(x+90, y+85);
        //             context.lineTo(x+90, y+25);
        //             context.lineTo(x+20, y+25);
        //            context.closePath();

        //            // Konva specific method
        //            context.fillStrokeShape(this);
        //          },
        //          fill: '#FFFFFF',
        //          stroke: 'black',
        //          strokeWidth: 2
        // });

        
        group.add(circle);
        group.add(innercircle);
        group.add(text);
        
        layer.add(group);
        //layer.add(tooltip);

        stage.add(layer);
    }

    function drawLines(beg, end){
        var line = new Konva.Line({
            points: [posX[beg], posY[beg], posX[end], posY[end]],
            stroke: 'black'
        });

        layer.add(line);
        stage.add(layer);
    }

    var tree = null;
    $("#set1").click(function(){
        tree = createRBTree();
        tree = tree.insert(40, '40');
        tree = tree.insert(50, '50');
        tree = tree.insert(100, '100');
        tree = tree.insert(200, '200');
        tree = tree.insert(300, '300');
        redrawTree();
    });

    $("#set2").click(function(){
        tree = createRBTree();
        tree = tree.insert(30, '30');
        tree = tree.insert(100,'100');
        tree = tree.insert(10, '10');
        redrawTree();
    });

    var st = "pause";
    var min = null;
    var loop = null;
    var running = false;
    var qtd = 0;
    $("#play").click(function(){
        if(tree != null && (tree.length >= 2 || running)){
            var image = document.getElementById('play');
            if(st == "pause"){//inicia
                st = "play";
                running = true;
                image.src = "img/pause.jpg";
                loop = setInterval(sum, 1000);
            }
            else{//pausa
                st = "pause";
                image.src = "img/play.png";
                clearInterval(loop);
            }
            $(".btn").hide();
            $("#bf").show();
            $("#value").hide();
            $("#remove").show();
        }
        else{
            alert(text4);
        }
    });
    $("#ff").click(function(){
        if(tree != null && (tree.length >= 2 || running) ){
            clearInterval(loop);
            sum();
            image = document.getElementById('play');
            st = "pause";
            running = true;
            image.src = "img/play.png";
            $("#bf").show();
            $(".btn").hide();
            $("#value").hide();
            $("#remove").show();
        }
        else{
            alert(text4);
        }
    });
    $("#add").click(function(){
        if(tree != null && tree.length > 5){
            alert(text5);
            return;
        }
        if(tree == null){
            tree = createRBTree();
        }
        val = parseInt($("#value").val());
        if(isNaN(val)){
            alert(text6);
            return;
        }
        tree = tree.insert(val, String(val));
        redrawTree();
    });

    $("#reset").click(function(){
        window.location="index.html";
    });

    $("#bf").click(function(){
        if(tree != null && (tree.length >= 2 || running)){
            clearInterval(loop);
            backTree();
            image = document.getElementById('play');
            st = "pause";
            image.src = "img/play.png";
            $(".btn").hide();
            $("#remove").show();
            //$("#bf").hide();
            $("#value").hide();
        }
        else{
            alert(text4);
        }
    });

    var removed = 0;
    var backuptree = null;
    function sum(){
        backupit = new backup();
        if(removed == 0){
            min = parseInt(getMin());
            oldmin = min;
            tree = tree.remove(String(min));
            removed = 1;
        }
        else if(min <= getMin()){
            oldmin = min;
            min = min + 1;
            $("#info").html(text2);
            backupit.textinfo = text2;
        }
        else{
            tree = tree.insert(min, String(min));
            removed = 0;
            $("#info").html(text3);
            backupit.textinfo = text2;
        }
        backupit.tree = tree;
        backupit.min = min;
        backupit.removed = removed;
        backupit.isrunning = running;
        backupit.state = st;
        backupit.lang = language;
        stack.push(backupit);
        redrawTree();
    }

    function backTree(){ //redo last change
        var backed = stack.pop();
        if(backed == null){
        }
        else{
            tree = backed.getTree();
            min = backed.getMin();
            removed = backed.getRemoved();
            st = backed.getState();
            running = backed.isRun();
            $("#info").html(String(backed.getTextinfo()));
            language = backed.getLang();
            redrawTree();
        }
    }

    function getMin(){
        node = tree.root;
        while(node.left != null && !isNaN(node.value)){
            node = node.left;
        }
        return node.value;
    }

    function redrawTree(){
        layer.clear();
        layer = new Konva.Layer();

        if(min == null) min = "null";
        //ctx.clearRect(0, 0, c.width, c.height);
        drawNode('#3366FF', posX[3]-100, posY[3]-200, min);


        root = tree.root;
        if(root != null && !isNaN(root.value)){
            if(root.left != null && !isNaN(root.left.value)){
                drawLines(0,1);
                aux = root.left;
                if(aux.left != null && !isNaN(aux.left.value)){
                    drawLines(1,3);
                }
                if(aux.right != null && !isNaN(aux.right.value)){
                    drawLines(1,4);
                }
            }
            if(root.right != null && !isNaN(root.right.value)){
                drawLines(0,2);
                aux = root.right;
                if(aux.left != null && !isNaN(aux.left.value)){
                    drawLines(2,5);
                }
                if(aux.right != null && !isNaN(aux.right.value)){
                    drawLines(2,6);
                }
            }
        }
        if(root != null && !isNaN(root.value)){
            if(root.left != null && !isNaN(root.left.value)){
                drawNode(colors[root.left._color], posX[1], posY[1],
                        root.left.value);
                aux = root.left;
                if(aux.left != null && !isNaN(aux.left.value)){
                    drawNode(colors[aux.left._color], posX[3], posY[3],
                            aux.left.value);
                }
                if(aux.right != null && !isNaN(aux.right.value)){
                    drawNode(colors[aux.right._color], posX[4], posY[4],
                            aux.right.value);
                }
            }
            if(root.right != null && !isNaN(root.right.value)){
                drawNode(colors[root.right._color], posX[2], posY[2],
                        root.right.value);
                aux = root.right;
                if(aux.left != null && !isNaN(aux.left.value)){
                    drawNode(colors[aux.left._color], posX[5], posY[5],
                            aux.left.value);
                }
                if(aux.right != null && !isNaN(aux.right.value)){
                    drawNode(colors[aux.right._color], posX[6], posY[6],
                            aux.right.value);
                }
            }
        }
        drawNode(colors[root._color], posX[0], posY[0], root.value);
    }
    $("#br").click(function(){
        language = 0;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                trans(xhttp);
            }
        }
        xhttp.open("GET", "xml/language.xml", true);
        xhttp.send();
    });
    $("#en").click(function(){
        language = 1;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                trans(xhttp);
            }
        }
        xhttp.open("GET", "xml/language.xml", true);
        xhttp.send();
    });
    function trans(xml) {
        xmlDoc = xml.responseXML;
        if(language == 0){
            x = xmlDoc.getElementsByTagName('text')[0];
            y = x.childNodes[0];
            text1 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('text')[1];
            y = x.childNodes[0];
            text2 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('text')[2];
            y = x.childNodes[0];
            text3 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('text')[5];
            y = x.childNodes[0];
            text4 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('text')[6];
            y = x.childNodes[0];
            text5 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('text')[7];
            y = x.childNodes[0];
            text6 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[0];
            y = x.childNodes[0];
            help1 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[1];
            y = x.childNodes[0];
            help2 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[2];
            y = x.childNodes[0];
            help3 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[3];
            y = x.childNodes[0];
            help4 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[4];
            y = x.childNodes[0];
            help5 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[5];
            y = x.childNodes[0];
            help6 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[6];
            y = x.childNodes[0];
            help7 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('tex')[0];
            y = x.childNodes[0];
            tex = y.nodeValue;
            x = xmlDoc.getElementsByTagName('run')[0];
            y = x.childNodes[0];
            run = y.nodeValue;
            x = xmlDoc.getElementsByTagName('title')[0];
            y = x.childNodes[0];
            title = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texth')[0];
            y = x.childNodes[0];
            textback = y.nodeValue;
        }
        else{
            x = xmlDoc.getElementsByTagName('text')[9];
            y = x.childNodes[0];
            text1 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('text')[10];
            y = x.childNodes[0];
            text2 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('text')[11];
            y = x.childNodes[0];
            text3 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('text')[14];
            y = x.childNodes[0];
            text4 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('text')[15];
            y = x.childNodes[0];
            text5 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('text')[16];
            y = x.childNodes[0];
            text6 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[1+6];
            y = x.childNodes[0];
            help1 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[2+6];
            y = x.childNodes[0];
            help2 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[3+6];
            y = x.childNodes[0];
            help3 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[4+6];
            y = x.childNodes[0];
            help4 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[5+6];
            y = x.childNodes[0];
            help5 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[6+6];
            y = x.childNodes[0];
            help6 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texthelp')[7+6];
            y = x.childNodes[0];
            help7 = y.nodeValue;
            x = xmlDoc.getElementsByTagName('tex')[1];
            y = x.childNodes[0];
            tex = y.nodeValue;
            x = xmlDoc.getElementsByTagName('run')[1];
            y = x.childNodes[0];
            run = y.nodeValue;
            x = xmlDoc.getElementsByTagName('title')[1];
            y = x.childNodes[0];
            title = y.nodeValue;
            x = xmlDoc.getElementsByTagName('texth')[1];
            y = x.childNodes[0];
            textback = y.nodeValue;
        }
        $("#add").val(text1);
        $("#remove").val(tex);
        $("#add").attr("title", help1);
        $("#play").attr("title", help2);
        $("#ff").attr("title", help3);
        $("#bf").attr("title", textback);
        $("#set1").attr("title", help4);
        $("#set2").attr("title", help4);
        $("#reset").attr("title", help5);
        $("#help").attr("title", help6);
        $("#remove").attr("title", help7);
        if(removed == 0){
        }
        else if(min <= getMin()){
            $("#info").html(text2);
        }
        else{
            $("#info").html(text3);
        }
        $("#info").attr("tittle", textoatual);
        document.title = title;
    }

    $("#help").click(function(){
        window.location = "index.html";
    });

    $("#remove").click(function(){
        val = $("#rm").val();
        if(isNaN(val)){
            alert(text6);
            return;
        }
        if(tree == null || tree.length == 0){
            alert(text4);
            return;
        }
        tree = tree.remove(val);
        redrawTree();
    });
});
